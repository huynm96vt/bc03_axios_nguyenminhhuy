function ValidatorND() {
  this.kiemTraRong = function (idTarget, idError, messageError) {
    let valueTarget = document.getElementById(idTarget).value.trim();
    if (!valueTarget) {
      document.getElementById(idError).innerText = messageError;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  };

  this.kiemTraIdHopLe = function (newNguoiDung, danhSachNguoiDung) {
    let index = danhSachNguoiDung.findIndex(function (item) {
      return item.taiKhoan == newNguoiDung.taiKhoan;
    });

    if (index == -1) {
      document.getElementById("spanUsername").innerText = "";
      return true;
    }
    document.getElementById("spanUsername").innerText =
      "Tài khoản không được trùng";
    return false;
  };


  this.kiemTraHoTen = function (idTarget, idError) {
    let valueInput = document.getElementById(idTarget).value;
    if (/[^a-zA-Z0-9\-\/]/.test(valueInput)) {
      document.getElementById(idError).innerText = "Họ tên không hợp lệ";
      return false;
    } else if (!/^([^0-9]*)$/.test(valueInput)) {
      document.getElementById(idError).innerText = "Họ tên không hợp lệ";
      return false;
    }
    document.getElementById(idError).innerText = "";
    return true;
  };

  this.kiemTraMatKhau = function (idTarget, idError) {
    let valueInput = document.getElementById(idTarget).value;
    if (
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]/.test(
        valueInput
      ) &&
      valueInput.length <= 8 &&
      valueInput.length >= 6
    ) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText = "Mật khẩu không hợp lệ";
    return false;
  };

  this.kiemTraEmail = function (idTarget, idError) {
    let parten = /^[a-z0-9](.?[a-z0-9]){5,}@g(oogle)?mail.com$/;
    let valueInput = document.getElementById(idTarget).value;

    if(parten.test(valueInput)){
      document.getElementById(idError).innerText = "";
      return true;
    }

    document.getElementById(idError).innerText = "Email không hợp lệ";
    return false;
  };

  this.kiemTraLoaiNguoiDung = function (idTarget, idError, messageError) {
    let valueTarget = document.getElementById(idTarget).value;
    if ((valueTarget != "GV") && (valueTarget != "HV")) {
      document.getElementById(idError).innerText = messageError;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  };

  this.kiemTraNgonNgu = function (idTarget, idError, messageError) {
    let valueTarget = document.getElementById(idTarget).value;
    if (valueTarget == "default") {
      document.getElementById(idError).innerText = messageError;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  };
  
  this.kiemTraMoTa = function (idTarget, idError, messageError) {
    let valueTarget = document.getElementById(idTarget).value.trim();
    if (valueTarget.length >= 60) {
      document.getElementById(idError).innerText = messageError;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  };
}
