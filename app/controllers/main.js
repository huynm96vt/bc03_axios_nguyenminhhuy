let danhSachNguoiDung = [];

let validatorND = new ValidatorND();

let idTemp = null;

const turnOnLoading = () => {
  document.getElementById("loading").style.display = "flex";
};

const turnOffLoading = () => {
  document.getElementById("loading").style.display = "none";
};

const BASE_URl = "https://62792677d00bded55ae50a6c.mockapi.io/user-info";

const renderDanhSachNguoiDungServ = () => {
  turnOnLoading();
  axios({
    url: `${BASE_URl}`,
    method: "GET",
  })
    .then((res) => {
      turnOffLoading();
      xuatDanhSachNguoiDung(res.data);
    })
    .catch((err) => {
      turnOffLoading();
    });
};

renderDanhSachNguoiDungServ();

const xoaNguoiDungServ = (id) => {
  turnOnLoading();
  axios({
    url: `${BASE_URl}/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      turnOffLoading();
      renderDanhSachNguoiDungServ();
    })
    .catch((err) => {
      turnOffLoading();
    });
};
const xuatDanhSachNguoiDung = (userList) => {
  let contentHTML = "";

  userList.forEach((item) => {
    let contentTr = `
        <tr>
        <td>${item.id}</td>
        <td>${item.taiKhoan}</td>
        <td>${item.matKhau}</td>
        <td>${item.hoTen}</td>
        <td>${item.email}</td>
        <td>${item.ngonNgu}</td>
        <td>${item.loaiND}</td>
        <td>
        <button class = "btn btn-danger" onclick = "xoaNguoiDungServ(${item.id})">Xoá</button>
        <button class = "btn btn-success" data-toggle="modal"
        data-target="#myModal" onclick = "layThongTinNguoiDung(${item.id})">Sửa</button>
        </td>
        </tr>
        `;
    contentHTML += contentTr;
  });

  document.getElementById("tblDanhSachNguoiDung").innerHTML = contentHTML;
};

const layThongTinTuForm = () => {
  let taiKhoan = document.getElementById("TaiKhoan").value;
  let hoTen = document.getElementById("HoTen").value;
  let matKhau = document.getElementById("MatKhau").value;
  let email = document.getElementById("Email").value;
  let hinhAnh = document.getElementById("HinhAnh").value;
  let loaiND = document.getElementById("loaiNguoiDung").value;
  let ngonNgu = document.getElementById("loaiNgonNgu").value;
  let moTa = document.getElementById("MoTa").value;

  return new NguoiDung(
    taiKhoan,
    hoTen,
    matKhau,
    email,
    loaiND == "HV" ? "Học viên" : "Giáo viên",
    ngonNgu,
    moTa,
    hinhAnh
  );
};

const themMoiND = () => {
  turnOnLoading();
  let newND = layThongTinTuForm();
  let isValidTaiKhoan =
    validatorND.kiemTraRong(
      "TaiKhoan",
      "spanUsername",
      "Tài khoản không được để rỗng"
    ) && validatorND.kiemTraIdHopLe(newND, danhSachNguoiDung);
  let isValidHoTen =
    validatorND.kiemTraRong("HoTen", "spanName", "Họ tên không được để rỗng") &&
    validatorND.kiemTraHoTen("HoTen", "spanName");
  let isValidPassword =
    validatorND.kiemTraRong(
      "MatKhau",
      "spanPass",
      "Mật khẩu không được để rỗng"
    ) && validatorND.kiemTraMatKhau("MatKhau", "spanPass");
  let isValidEmail =
    validatorND.kiemTraRong("Email", "spanEmail", "Email không được để rỗng") &&
    validatorND.kiemTraEmail("Email", "spanEmail");
  let isValidHinhAnh = validatorND.kiemTraRong(
    "HinhAnh",
    "spanImg",
    "Hình ảnh không được để rỗng"
  );
  let isValidLoaiNguoiDung = validatorND.kiemTraLoaiNguoiDung(
    "loaiNguoiDung",
    "spanType",
    "Loại người dùng không được để rỗng"
  );
  let isValidNgonNgu = validatorND.kiemTraNgonNgu(
    "loaiNgonNgu",
    "spanLanguage",
    "Ngôn ngữ không được để rỗng"
  );
  let isValidMoTa =
    validatorND.kiemTraRong(
      "MoTa",
      "spanDescription",
      "Mô tả không được để rỗng"
    ) &&
    validatorND.kiemTraMoTa("MoTa", "spanDescription", "Mô tả không hợp lệ");
  let isValid =
    isValidTaiKhoan &&
    isValidHoTen &&
    isValidPassword &&
    isValidEmail &&
    isValidHinhAnh &&
    isValidLoaiNguoiDung &&
    isValidNgonNgu &&
    isValidMoTa;
  if (isValid) {
    axios({
      url: `${BASE_URl}`,
      method: "POST",
      data: newND,
    })
      .then((res) => {
        turnOffLoading();
        $("#myModal").modal("hide");
        danhSachNguoiDung.push(newND);
        renderDanhSachNguoiDungServ();
      })
      .catch((err) => {
        turnOffLoading();
      });
  }
};

const renderThongTinLenForm = (item) => {
  document.getElementById("TaiKhoan").value = item.taiKhoan;
  document.getElementById("HoTen").value = item.hoTen;
  document.getElementById("MatKhau").value = item.matKhau;
  document.getElementById("Email").value = item.email;
  document.getElementById("HinhAnh").value = item.hinhAnh;
  document.getElementById("loaiNguoiDung").value = item.loaiND;
  document.getElementById("loaiNgonNgu").value = item.ngonNgu;
  document.getElementById("MoTa").value = item.moTa;
};

const layThongTinNguoiDung = (id) => {
  turnOnLoading();
  idTemp = id;
  document.getElementById("btn-add").style.display = "none";
  document.getElementById("btn-update").style.display = "block";
  axios({
    url: `${BASE_URl}/${id}`,
    method: "GET",
  })
    .then((res) => {
      turnOffLoading();
      renderThongTinLenForm(res.data);
    })
    .catch((err) => {
      turnOffLoading();
    });
};

const capNhatND = () => {
  turnOnLoading();
  let updatedInfo = layThongTinTuForm();
  axios({
    url: `${BASE_URl}/${idTemp}`,
    method: "PUT",
    data: updatedInfo,
  })
    .then((res) => {
      turnOffLoading();
      $("#myModal").modal("hide");
      renderDanhSachNguoiDungServ(res.data);
    })
    .catch((err) => {
      turnOffLoading();
    });
};

document.getElementById("btnThemNguoiDung").addEventListener("click", function () {
    document.getElementById("btn-add").style.display = "block";
    document.getElementById("btn-update").style.display = "none";
    document.getElementById("form-nd").reset();
  });

const timKiem = () => {
  let inputTimKiem = document.getElementById("inputSearch").value;
  turnOnLoading();
  axios({
    url: `${BASE_URl}`,
    method: "GET",
  })
    .then((res) => {
      turnOffLoading();
      let taikhoanList = res.data;
      let taiKhoanTimKiem = taikhoanList.filter((item) => {
        return item.taiKhoan == inputTimKiem;
      });
      xuatDanhSachNguoiDung(taiKhoanTimKiem);
    })
    .catch((err) => {
      turnOffLoading();
    });
};
